package com.ayge.ayge_fragments.Adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.ayge.ayge_fragments.DetallesPeliculaActivity
import com.ayge.ayge_fragments.Model.Pelicula
import com.ayge.ayge_fragments.R
import kotlinx.android.synthetic.main.item_pelicula.view.*

class AdaptadorRecycler(
    val peliculas: MutableList<Pelicula>,
    val context: Context?
) : RecyclerView.Adapter<ViewHolder>() {


    private var onItemPeliculaSelectedListener: (peliculas: Pelicula) -> Unit = { }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = peliculas.get(position)
        context?.let { holder.bind(item, it) }

        holder.setPelicula(item)
        holder.setOnItemPeliculaSelectedListener(onItemPeliculaSelectedListener)
    }

    public fun setOnItemPeliculaSelected(listener: (peliculas: Pelicula)->Unit){
        this.onItemPeliculaSelectedListener = listener
    }


    public fun addPeliculaAtPosition(peliculas: Pelicula, position: Int){
        this.peliculas.add(position ,peliculas)
        notifyItemInserted(position)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.item_pelicula,
                parent,
                false
            )
        )

    }

    override fun getItemCount(): Int {
        return peliculas.size
    }

}

class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val iv_pelicula = view.iv_rv_imagen
    val tv_titulo = view.tv_rv_pelicula
    val tv_idioma = view.tv_rv_idioma
    val tv_id = view.tv_id
    private lateinit var pelicula: Pelicula

    fun bind(pelicula: Pelicula, context: Context) {
        var id = pelicula.id
        when (id) {
            1 -> {
                iv_pelicula.setImageResource(R.drawable.increibles)
            }
            2 -> {
                iv_pelicula.setImageResource(R.drawable.mary_poppins)
            }
            3 -> {
                iv_pelicula.setImageResource(R.drawable.mascotas2)
            }
            4 -> {
                iv_pelicula.setImageResource(R.drawable.villano)
            }
        }
        tv_titulo?.text = pelicula.pelicula
        tv_idioma?.text = pelicula.idioma

        itemView.setOnClickListener(View.OnClickListener {
            val detalleIntent = Intent(context, DetallesPeliculaActivity::class.java)
            detalleIntent.putExtra("pelicula", pelicula)
            itemView.context.startActivity(detalleIntent)
            Toast.makeText(
                context,
                "pelicula seleccionada " + tv_titulo.text,
                Toast.LENGTH_SHORT
            ).show()

        })

    }

    public fun setPelicula(pelicula: Pelicula){
        this.pelicula = pelicula
    }

    fun setOnItemPeliculaSelectedListener(listener: (pelicula: Pelicula) -> Unit){
        itemView.setOnClickListener{listener(pelicula)}
    }
}

