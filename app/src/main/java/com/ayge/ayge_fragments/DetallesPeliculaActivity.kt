package com.ayge.ayge_fragments

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.ayge.ayge_fragments.Model.Pelicula

const val idDetalles= "Detalles"

class DetallesPeliculaActivity : AppCompatActivity() {
    lateinit var pelicula: Pelicula

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detalles)

        pelicula = intent.extras?.getSerializable("peliParam") as Pelicula


        if(savedInstanceState == null){
            supportFragmentManager.beginTransaction().add( R.id.container_detalles, DetallesPeliculaFragment.newInstance(pelicula), idDetalles).commit()
        }


    }




}
