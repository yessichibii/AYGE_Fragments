package com.ayge.ayge_fragments

import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ayge.ayge_fragments.Model.Pelicula
import kotlinx.android.synthetic.main.fragment_detalles.*

private const val CONTACT_PARAM = "peliParam"


class DetallesPeliculaFragment : Fragment() {
    private var pelicula:Pelicula? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            pelicula = it.getSerializable(CONTACT_PARAM) as Pelicula
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detalles, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val idioma = pelicula?.idioma
        val titulo = pelicula?.pelicula
        val id = pelicula!!.id;

        when(id){
            1 -> {iv_det_imagen.setImageResource(R.drawable.increibles)}
            2 -> {iv_det_imagen.setImageResource(R.drawable.mary_poppins)}
            3 -> {iv_det_imagen.setImageResource(R.drawable.mascotas2)}
            4 -> {iv_det_imagen.setImageResource(R.drawable.villano)}
        }
        tv_det_idioma?.text = idioma
        tv_det_titulo?.text = titulo


    }


    companion object {
        @JvmStatic
        fun newInstance(pelicula: Pelicula) =
            DetallesPeliculaFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(CONTACT_PARAM, pelicula)
                }
            }

    }
}
