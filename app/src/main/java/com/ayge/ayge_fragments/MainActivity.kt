package com.ayge.ayge_fragments

import android.content.Intent
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem

import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.toolbar
import kotlinx.android.synthetic.main.activity_registro.*

const val idMain= "Main"
const val RESULT_SAVE = 0x2334

class MainActivity : AppCompatActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        if(savedInstanceState == null){
            supportFragmentManager.beginTransaction().add(R.id.container_main,MainFragment.newInstance(), idMain).commit()
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        val mainFragment = supportFragmentManager.findFragmentByTag(idMain)
        mainFragment?.onActivityResult(requestCode,resultCode,data)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_registro -> {
                val registrarPeliculaIntent = Intent(this,RegistroActivity::class.java)
   //             registrarPeliculaIntent.putExtra("peliParam", pelicula)
                startActivityForResult(registrarPeliculaIntent,
                    RESULT_SAVE
                )
                true
            }
            else -> super.onOptionsItemSelected(item)

        }
    }
}
