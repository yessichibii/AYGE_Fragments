package com.ayge.ayge_fragments

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.ayge.ayge_fragments.Adapter.AdaptadorRecycler
import com.ayge.ayge_fragments.Model.Pelicula
import kotlinx.android.synthetic.main.fragment_main.*


class MainFragment : Fragment() {
    private lateinit var peliculasAdapter: AdaptadorRecycler
    private var isTablet = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isTablet = resources.getBoolean(R.bool.isTablet)





    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val linearLayoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)

        rvLista.layoutManager = linearLayoutManager
        rvLista.setHasFixedSize(true)

        var peliculas = getPelicula()

        peliculasAdapter = AdaptadorRecycler(peliculas, context)
        peliculasAdapter.setOnItemPeliculaSelected{
            if (isTablet) {
                activity?.supportFragmentManager!!.beginTransaction().replace(R.id.container_detalles,
                    DetallesPeliculaFragment.newInstance(it)).commit()
            } else {
                val detallesIntent = Intent(activity, DetallesPeliculaActivity::class.java)
                detallesIntent.putExtra("peliParam", it)
                startActivity(detallesIntent)
            }
        }
        rvLista.adapter = peliculasAdapter

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == RESULT_SAVE){
            val peliculas = data?.getSerializableExtra("pelicula") as Pelicula?
            peliculas?.let{
                peliculasAdapter.addPeliculaAtPosition(peliculas,0)
            }

        }
    }

    companion object {

        @JvmStatic
        fun newInstance() = MainFragment()
    }

    fun getPelicula(): MutableList<Pelicula> {
        var peliculas:MutableList<Pelicula> = ArrayList()
        peliculas.add(Pelicula(1, "Los Increibles", "Español", "increibles"))
        peliculas.add(Pelicula(2, "Mary Poppins 2", "Español", "mary_poppins"))
        peliculas.add(Pelicula(3, "La vida secreta de mis mascotas 2", "Español", "mascotas2"))
        peliculas.add(Pelicula(4, "Mi villano Favorito 3", "Español", "villano"))
        return peliculas
    }

}
