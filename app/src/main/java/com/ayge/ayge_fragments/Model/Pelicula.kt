package com.ayge.ayge_fragments.Model

import java.io.Serializable

data class Pelicula (
    var id: Int,
    var pelicula: String,
    var idioma: String,
    var nombreImagen: String
): Serializable
