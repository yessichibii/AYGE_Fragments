package com.ayge.ayge_fragments

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.ayge.ayge_fragments.Model.Pelicula
import kotlinx.android.synthetic.main.activity_registro.*
const val idRegistro = "Registro" // Para cuando se agregue a la pila de fragmentes, este sera el identificador del fragmento metido


class RegistroActivity : AppCompatActivity() {

    lateinit var pelicula: Pelicula



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registro)

//        pelicula = intent.extras?.getSerializable("peliParam") as Pelicula
 //       setTitle("hj")
        if(savedInstanceState == null){
            supportFragmentManager.beginTransaction().add( R.id.container_registro, RegistroFragment.newInstance(),idRegistro).commit()
        }

        fab_pagar.setOnClickListener { view ->
            val registrarFragment = supportFragmentManager.findFragmentByTag(idRegistro) as RegistroFragment
            registrarFragment.setOnClickListener(view)
        }

    }



}
