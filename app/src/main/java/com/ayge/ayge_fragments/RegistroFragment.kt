package com.ayge.ayge_fragments

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.CompoundButton
import android.widget.Toast
import com.ayge.ayge_fragments.Model.Pelicula
import kotlinx.android.synthetic.main.activity_registro.*
import kotlinx.android.synthetic.main.fragment_registro.*

private const val CONTACT_PARAM = "peliParam"
class RegistroFragment : Fragment() {

    var idioma  = "español"
    var factura = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       /* arguments?.let {
            peliculaImagen = it.getSerializable(CONTACT_PARAM) as Pelicula
        }*/
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_registro, container, false)
    }



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
/*
        val id = peliculaImagen?.id;

        when(id){
            1 -> {iv_pelicula.setImageResource(R.drawable.increibles)}
            2 -> {iv_pelicula.setImageResource(R.drawable.mary_poppins)}
            3 -> {iv_pelicula.setImageResource(R.drawable.mascotas2)}
            4 -> {iv_pelicula.setImageResource(R.drawable.villano)}
        }
        */
        iv_pelicula.setImageResource(R.drawable.increibles)
        val idiomas = arrayListOf<String>("Español", "Subtitulado", "Original")
        val idiomaAdapter = ArrayAdapter<String>(activity!!,android.R.layout.simple_spinner_item, idiomas)
        idiomaAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item) // Ocupa una vista por default
        sp_idiomas.adapter=idiomaAdapter

        sp_idiomas.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                idioma = idiomas[position]
            }

            override fun onNothingSelected(arg0: AdapterView<*>) {

            }
        })

        var tipoCliente = "Estandar"
        rg_tipo_cliente.setOnCheckedChangeListener{group, checkedId ->
            when(checkedId){
                rb_normal.id ->
                    tipoCliente = "Estandar"
                rb_premium.id ->
                    tipoCliente = "Premium"
            }
        }


        chb_factura.setOnCheckedChangeListener({ buttonView, isChecked ->
            factura = isChecked })

        ib_trailer.setOnClickListener(View.OnClickListener {
            val id = "6RBr1v9_umA"
            val intentApp = Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:$id"))
            val intentNavegador = Intent(
                Intent.ACTION_VIEW,
                Uri.parse("http://www.youtube.com/watch?v=$id")
            )
            try {
                startActivity(intentApp)
            } catch (ex: ActivityNotFoundException) {
                startActivity(intentNavegador)
            }

        })


        til_nombre.editText?.setOnFocusChangeListener(View.OnFocusChangeListener { view, hasFocus ->
            val nombre=til_nombre.editText?.text.toString()

            if (!hasFocus){
                if (nombre.isNotEmpty()) {
                    til_nombre.isErrorEnabled = false
                }else {
                    til_nombre.isErrorEnabled = true
                    til_nombre.error = "El nombre no puede quedar vacio"
                }
            }
        })

        til_apellidos.editText?.setOnFocusChangeListener(View.OnFocusChangeListener { view, hasFocus ->
            val apellidos=til_apellidos.editText?.text.toString()

            if (!hasFocus){
                if (apellidos.isNotEmpty()) {
                    til_apellidos.isErrorEnabled = false
                }else {
                    til_apellidos.isErrorEnabled = true
                    til_apellidos.error = "Los apellidos no pueden ser vacios"
                }
            }
        })

        til_direccion.editText?.setOnFocusChangeListener(View.OnFocusChangeListener { view, hasFocus ->
            val direccion=til_direccion.editText?.text.toString()
            if (!hasFocus){
                if (direccion.isNotEmpty()) {
                    til_direccion.isErrorEnabled = false
                }else {
                    til_direccion.isErrorEnabled = true
                    til_direccion.error = "La direccion no puede ser vacia"
                }
            }
        })

    }

    public fun setOnClickListener(view: View){

        if(til_nombre.editText?.text.toString().isEmpty() || til_apellidos.editText?.text.toString().isEmpty() || til_direccion.editText?.text.toString().isEmpty()){
                Toast.makeText(activity!!,"Debe llenar todos los campos", Toast.LENGTH_LONG).show()
        }else {
            val pelicula = Pelicula(1,til_nombre.editText?.text.toString(),idioma,"increibles")

            val intent = Intent()
            intent.putExtra("pelicula" ,pelicula)
            activity?.setResult(RESULT_SAVE, intent)
            activity?.finish()

        }

    }


    companion object {

        @JvmStatic
        fun newInstance() = RegistroFragment()
     /*   fun newInstance(pelicula: Pelicula) =
            RegistroFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(CONTACT_PARAM, pelicula)
                }
            }
*/
    }

    private val onCheckedChangeListener: CompoundButton.OnCheckedChangeListener =
        CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
            factura = isChecked }

}
